var app = require('express').createServer()
   , io = require('socket.io').listen(app.listen(3000));

app.get('/', function (req, res) {
    res.sendfile(__dirname + '/index.html');
});

// array che contiene gli utenti collegati
var users = [];

io.sockets.on('connection', function (socket) {

  // si mette in ascolto dell'evento message 
  socket.on('message',function(data,time,username){

    // io.sockets.emit manda il messaggio a TUTTI
    // io.sockets.emit('message',data);

    // socket.broadcast.emit manda il messaggio a tutti tranni al client che ha emesso l'evento
    socket.broadcast.emit('message',data,time,username);

    socket.emit('message',data,time,username,true);
  });

  // si mette in ascolto per l'evento connected
  socket.on('connected',function(username){

    // salvo l'username nell'oggetto
    socket.nick = username;

    // salvo l'username nell'array
    users.push(username);

    // emetto l'evento che aggiorna gli utenti collegati
    io.sockets.emit('user_connected',users);
  });

  // in ascolto per l'evento disconnect, da notare che questo evento viene emesso automaticamente quando un client chiude il browser
  socket.on('disconnect',function(){

    // rimuovo l'utente dall'array
    users.splice(users.indexOf(socket.nick), 1);

    // mando la lista aggiornata degli utenti ai client
    socket.broadcast.emit('updated_user_list',users);
  });
  
});